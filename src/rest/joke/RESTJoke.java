/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.joke;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// Obtener jar de: http://central.maven.org/maven2/org/json/json/20180813/json-20180813.jar
import org.json.JSONObject;

//Obtener jar de: http://central.maven.org/maven2/org/apache/commons/commons-text/1.6/commons-text-1.6.jar
import org.apache.commons.text.StringEscapeUtils;
//Incluir en Libraries tambien el jar: http://central.maven.org/maven2/org/apache/commons/commons-lang3/3.8/commons-lang3-3.8.jar
/**
 *
 * @author molguin
 */
public class RESTJoke {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            URL url = new URL("http://api.icndb.com/jokes/random");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);

            int status = con.getResponseCode();
            
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            
            String inputLine;
            StringBuilder content = new StringBuilder();
            
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            
            JSONObject jsonObj = new JSONObject(content.toString());
            String joke = StringEscapeUtils.unescapeHtml3(
                    jsonObj.getJSONObject("value").getString("joke")
            );
            
            in.close();
            
            con.disconnect();
     
            System.out.println(joke);

        } catch (MalformedURLException ex) {
            Logger.getLogger(RESTJoke.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RESTJoke.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
